# FilmaffinitySeleniumProject
This project demonstrates how to use web scraping to get movie information from Filmaffinity using Python Selenium and the Page Object Model design pattern.


## Getting started
To run this project, you will need the following software:
* Python 3.8.10
* pip (Python package manager)
* [Selenium](https://www.selenium.dev/downloads/)
* [ChromeDriver](https://chromedriver.chromium.org/downloads) or [GeckoDriver](https://github.com/mozilla/geckodriver/releases) (depending on which web browser you want to use)

You can install these packages by running the following command:


## Virtual Environment

It is recommended to create a virtual environment for this project to isolate the Python libraries and packages that are used. This will prevent conflicts with any other Python packages that you have installed on your system.

To create a virtual environment, you can use the `venv` module that is included in Python 3.x.

First, go to the project folder and navigate to it: 

    cd filmaffinity_selenium_project

Next, create a new virtual environment with the following command:

    python -m venv env

This will create a new directory called `env` that contains the Python executable and the necessary files for the virtual environment.

To activate the virtual environment, run the following command:

`source env/bin/activate` or just `env\Scripts\active`

Your virtual environment is now active, and you can install the necessary Python packages with the `pip` command.

    pip install selenium
    pip install pyunitreport


To deactivate the virtual environment, simply run the `deactivate` command:

    deactivate

You can also use a tool like [virtualenv](https://virtualenv.pypa.io/en/latest/) or [conda](https://docs.conda.io/en/latest/) to create and manage virtual environments. Choose the one that works best for you.


## Project Structure

The project is structured as follows:

    filmaffinity_selenium_project/
        ├── pages/
        │   ├── __init__.py
        │   ├── enumerators.py
        │   ├── home_page.py
        │   ├── movie_page.py
        │   └── search_results_page.py
        └── tests/
            ├── __init__.py
            └── test_scrape_movie_info.py
            └── test_search_page.py

-   `pages`: This directory contains the Page Object Model classes that represent the different pages of the Filmaffinity website.
-   `tests`: This directory contains the classes to scrape movie information from Filmaffinity.


## Running the Tests

To run the tests, navigate to the `tests` directory and run the following command:

    python test_scrape_movie_info.py


This will run the test case that scrapes movie information from Filmaffinity and prints it to the console.

## Page Object Model

The Page Object Model (POM) is a design pattern that separates the representation of a page from the actions that can be performed on it. This separation of concerns makes it easier to maintain and update the tests as the page design changes.

In this project, each page of the Filmaffinity website is represented by a separate POM class. For example, the `HomePage` class represents the homepage of the website, and the `SearchResultsPage` class represents the page that displays the search results.

The POM classes contain methods that perform actions on the page, such as searching for a movie or navigating to a specific movie page. They also contain attributes that represent the elements on the page, such as the search box or the movie titles.

By using the POM design pattern, the test cases can interact with the page objects in a natural and intuitive way, without having to worry about the underlying HTML structure of the page.

## Scraping Movie Info

To scrape movie information from Filmaffinity, the test case uses Python Selenium to automate the Chrome or Firefox web browser. It navigates to the homepage and performs a search for a specific movie. It then navigates to the movie page and extracts the desired information, such as the title, rating, and plot summary.

Finally, the test case prints the movie information to the console.

## Additional Resources

-   [Filmaffinity](https://www.filmaffinity.com/)
-   [Selenium documentation](https://www.selenium.dev/documentation/en/)
-   [ChromeDriver](https://chromedriver.chromium.org/downloads)
-   [GeckoDriver](https://github.com/mozilla/geckodriver/releases)