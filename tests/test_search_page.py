import sys
import unittest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Setting path. Not a good practice but didnt find a solution in POM structures.
sys.path.append('../filmaffinity_selenium_project')
from pages.home_page import HomePage
from pages.search_results_page import SearchResultsPage
from pages.movie_page import MoviePage
from pages.enumerators import SearchSortBy

class TestSearchPage(unittest.TestCase):
    
    def setUp(self):
        user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:108.0) Gecko/20100101 Firefox/108.0"
        options = Options()
        options.add_argument(f"user-agent={user_agent}")

        self.driver = webdriver.Firefox(options=options)
        self.driver.maximize_window()

    
    def tearDown(self):
        self.driver.quit()
    
    def test_empty_result_search(self):

        home_page = HomePage(self.driver)
        search_results_page = home_page.search_for_movie("'")
        expected_title_count = 0
        movie_titles = search_results_page.get_movie_titles()
        self.assertEqual(expected_title_count, len(movie_titles))
   
    def test_see_all_result_search(self):

        expected_movie_number = 5
        expected_all_movie_number = 22
        home_page = HomePage(self.driver)
        
        search_results_page = home_page.search_for_movie('The Godfather')
        movie_number = len(search_results_page.get_movie_titles())
        self.assertEqual(expected_movie_number, movie_number)
        
        search_results_page.scroll_to_botton_and_wait(3)
        search_results_page.see_all_results()
        movie_number = len(search_results_page.get_movie_titles())
        self.assertEqual(expected_all_movie_number, movie_number)
    
    def test_sort_search_list(self):
        
        expected_movie_titles_by_relevance = ['Terminator', 'Terminator 2: El juicio final', 'Terminator 3: La rebelión de las máquinas', 'Terminator Salvation', 'Terminator Génesis', 'Terminator: Destino oscuro', 'Terminator: Resistance', 'Terminator: Las crónicas de Sarah Connor (Serie de TV)', 'J.T. LeRoy: engañando a Hollywood', 'T2: Reprogramming The Terminator', '8 Bit Cinema: Terminator 2 (C)', 'Batman versus The Terminator (C)', 'Terminator Fury', 'T2 3-D: La batalla a través del tiempo (C)', 'Terminator Salvation: The Machinima Series (Miniserie de TV)', 'Terminator 2 (Shocking Dark)', 'Terminator Woman', 'Lady Terminator', 'Ninja Terminator', "Otras voces: Creando 'Terminator'", 'Así se hizo "Terminator 2: el juicio final"', 'Mars, el exterminador (Serie de TV)', "The Making of 'Terminator 2 3D' (C)", 'Black Samurai', 'Alien Terminator', 'Kickboxer Terminator', 'El hombre de blanco', 'Top Line', 'Cute Foster Sister']
        expected_movie_titles_by_year = ['Terminator: Destino oscuro', 'Terminator: Resistance', 'J.T. LeRoy: engañando a Hollywood', 'T2: Reprogramming The Terminator', 'Terminator Génesis', '8 Bit Cinema: Terminator 2 (C)', 'Terminator Fury', 'Batman versus The Terminator (C)', 'Terminator Salvation', 'Terminator Salvation: The Machinima Series (Miniserie de TV)', 'Terminator: Las crónicas de Sarah Connor (Serie de TV)', 'Terminator 3: La rebelión de las máquinas', 'Mars, el exterminador (Serie de TV)', "Otras voces: Creando 'Terminator'", "The Making of 'Terminator 2 3D' (C)", 'T2 3-D: La batalla a través del tiempo (C)', 'Alien Terminator', 'El hombre de blanco', 'Kickboxer Terminator', 'Terminator 2: El juicio final', 'Terminator Woman', 'Así se hizo "Terminator 2: el juicio final"', 'Terminator 2 (Shocking Dark)', 'Lady Terminator', 'Top Line', 'Ninja Terminator', 'Terminator', 'Cute Foster Sister', 'Black Samurai']
        home_page = HomePage(self.driver)
        
        search_results_page = home_page.search_for_movie('Terminator')
        search_results_page.scroll_to_botton_and_wait(3)
        search_results_page.see_all_results()
        movie_titles = search_results_page.get_movie_titles()
        self.assertListEqual(expected_movie_titles_by_relevance, movie_titles)
        
        search_results_page.sort_results(SearchSortBy.YEAR)
        movie_titles = search_results_page.get_movie_titles()
        self.assertListEqual(expected_movie_titles_by_year, movie_titles)

        search_results_page.sort_results(SearchSortBy.RELEVANCE)
        movie_titles = search_results_page.get_movie_titles()
        self.assertListEqual(expected_movie_titles_by_relevance, movie_titles)


if __name__ == '__main__':
    unittest.main()