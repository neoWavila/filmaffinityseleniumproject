import sys
import unittest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

# Setting path. Not a good practice but didnt find a solution in POM structures.
sys.path.append('../filmaffinity_selenium_project')
from pages.home_page import HomePage
from pages.search_results_page import SearchResultsPage
from pages.movie_page import MoviePage
from pages.enumerators import SearchSortBy

class TestScrapeMovieInfo(unittest.TestCase):
    
    def setUp(self):
        user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:108.0) Gecko/20100101 Firefox/108.0"
        options = Options()
        options.add_argument(f"user-agent={user_agent}")

        self.driver = webdriver.Firefox(options=options)
        self.driver.maximize_window()
    
    def tearDown(self):
        self.driver.quit()
    
    def test_scrape_movie_info_by_search_page_first_result(self):

        home_page = HomePage(self.driver)
        search_results_page = home_page.search_for_movie('Inception')
        movie_titles = search_results_page.get_movie_titles()
        print(movie_titles)
        
        movie_page = search_results_page.open_movie_page(movie_titles[0])
        expected_local_title = "Origen"
        expected_title = "Inception"
        expected_year = "2010"
        expected_director = "Christopher Nolan"
        expected_genres = ['Ciencia ficción', 'Thriller', 'Intriga', 'Acción']
        expected_plot = "Dom Cobb (DiCaprio) es un experto en el arte de apropiarse, durante el sueño, de los secretos del subconsciente ajeno. La extraña habilidad de Cobb le ha convertido en un hombre muy cotizado en el mundo del espionaje, pero también lo ha condenado a ser un fugitivo y, por consiguiente, a renunciar a llevar una vida normal. Su única oportunidad para cambiar de vida será hacer exactamente lo contrario de lo que ha hecho siempre: la incepción, que consiste en implantar una idea en el subconsciente en lugar de sustraerla. Sin embargo, su plan se complica debido a la intervención de alguien que parece predecir cada uno de sus movimientos, alguien a quien sólo Cobb podrá descubrir. (FILMAFFINITY)"
        expected_rating = "8,0"

        self.assertEqual(expected_local_title, movie_page.get_local_language_title())
        self.assertEqual(expected_title, movie_page.get_title())
        self.assertEqual(expected_year, movie_page.get_year())
        self.assertEqual(expected_director, movie_page.get_director())
        self.assertEqual(expected_genres, movie_page.get_genre())
        self.assertEqual(expected_plot, movie_page.get_plot())
        self.assertEqual(expected_rating, movie_page.get_rating())
    
if __name__ == '__main__':
    unittest.main()