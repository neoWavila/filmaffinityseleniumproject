from enum import Enum
from selenium.webdriver.common.by import By

class SearchSortBy(Enum):
    RELEVANCE = (By.CSS_SELECTOR, '.button-group > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)')
    YEAR = (By.CSS_SELECTOR, '.button-group > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)')
