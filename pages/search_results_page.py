from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from time import sleep
from .movie_page import MoviePage
from .enumerators import SearchSortBy

class SearchResultsPage:

    def __init__(self, driver):
        self.driver = driver
        self.__DEFAULT_WAIT_TIME = 10
        self.__MOVIE_TITLES_LOCATOR = (By.CSS_SELECTOR, '.mc-title a')
        self.__SEE_ALL_BUTTON_LOCATOR = (By.CLASS_NAME, 'see-all-button')
    
    def get_movie_titles(self):
        movie_title_text_list = []
        movie_title_list = []
        
        try:
            movie_title_list = WebDriverWait(self.driver, self.__DEFAULT_WAIT_TIME).until(EC.presence_of_all_elements_located(self.__MOVIE_TITLES_LOCATOR))
        except TimeoutException:
            return movie_title_text_list
        
        for title in movie_title_list:
            movie_title_text_list.append(title.text)

        return movie_title_text_list

    def see_all_results(self):
        button = WebDriverWait(self.driver, self.__DEFAULT_WAIT_TIME).until(EC.element_to_be_clickable(self.__SEE_ALL_BUTTON_LOCATOR)).click()

    def sort_results(self, sort_option: SearchSortBy):
        WebDriverWait(self.driver, self.__DEFAULT_WAIT_TIME).until(EC.element_to_be_clickable(sort_option.value)).click()

        
    def open_movie_page(self, movie_title):
        WebDriverWait(self.driver, self.__DEFAULT_WAIT_TIME).until(EC.element_to_be_clickable((By.CSS_SELECTOR,  f'a[title="{movie_title} "]'))).click()
        return MoviePage(self.driver)
    
    def scroll_to_botton_and_wait(self, sleepTime):
        self.driver.execute_script("window.scrollBy(0,document.documentElement.scrollHeight)")
        sleep(sleepTime) #EC.visibility_of_element_located doesn't look to work because its condition completes before finish the scroll.