from selenium.webdriver.common.by import By

class MoviePage:

    def __init__(self, driver):
        self.driver = driver
        
        self.__LOCAL_LANGUAGE_TITLE_LOCATOR = (By.ID, "main-title")
        self.__RATING_LOCATOR = (By.ID, 'movie-rat-avg')
        
        self.__MOVIE_INFO_LOCATOR = (By.CSS_SELECTOR, ".movie-info dd")
        self.__MOVIE_INFO_ROWS = driver.find_elements(*self.__MOVIE_INFO_LOCATOR)
        self.__TITLE_ROW = 0
        self.__YEAR_ROW = 1
        self.__RUNTIME_ROW = 2
        self.__DIRECTOR_ROW = 4
        self.__GENRE_AND_TOPIC_ROW = 10
        self.__PLOT_ROW = 11

    def get_local_language_title(self):
        return self.driver.find_element(*self.__LOCAL_LANGUAGE_TITLE_LOCATOR).text
    
    def get_title(self):
        return self.__MOVIE_INFO_ROWS[self.__TITLE_ROW].text
    
    def get_year(self):
        return self.__MOVIE_INFO_ROWS[self.__YEAR_ROW].text
    
    def get_runtime(self):
        return self.__MOVIE_INFO_ROWS[self.__RUNTIME_ROW].text
    
    def get_director(self):
        return self.__MOVIE_INFO_ROWS[self.__DIRECTOR_ROW].text
    
    def get_genre(self):
        movie_genre_text = self.__MOVIE_INFO_ROWS[self.__GENRE_AND_TOPIC_ROW].text.split("|")[0]
        movie_genre_list = movie_genre_text.split(".")
        striped_movie_genre_list = []

        for movie_genre in movie_genre_list:
            movie_genre = movie_genre.lstrip(' ')
            movie_genre = movie_genre.rstrip(' ')
            striped_movie_genre_list.append(movie_genre)

        return striped_movie_genre_list
    
    def get_plot(self):
        return self.__MOVIE_INFO_ROWS[self.__PLOT_ROW].text
    
    def get_rating(self):
        return self.driver.find_element(*self.__RATING_LOCATOR).text