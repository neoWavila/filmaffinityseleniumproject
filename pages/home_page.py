from selenium import webdriver
from selenium.webdriver.common.by import By
from .search_results_page import SearchResultsPage

class HomePage:

    def __init__(self, driver):
        self.driver = driver
        self.__URL = 'https://www.filmaffinity.com'
        self.__SEARCH_BOX_LOCATOR = (By.ID, 'top-search-input')
        self.__SEARCH_BUTTON_LOCATOR = (By.ID, 'button-search')
        self.__PRIVACY_ACCPET_BUTTON_LOCATOR = (By.XPATH, '//*[@id="qc-cmp2-ui"]/div[2]/div/button[2]/span')

    def navigate(self):
        self.driver.get(self.__URL)
        self.driver.find_element(*self.__PRIVACY_ACCPET_BUTTON_LOCATOR).click()
    
    def search_for_movie(self, movie_name):
        self.navigate()
        search_bar = self.driver.find_element(*self.__SEARCH_BOX_LOCATOR)
        search_bar.clear()
        search_bar.send_keys(movie_name)
        self.driver.find_element(*self.__SEARCH_BUTTON_LOCATOR).click()
        return SearchResultsPage(self.driver)